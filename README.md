# 🗺️ _Perlin noise_ (mapa procedural) 🗺️
Generación procedural de mapa de sonido Perlin (_perlin noise_). Consiste en la utilización de una función procedural matemática con tal de generar un mapa con biomas que se distribuyen de forma aleatoria a través de unos parámetros configurados por el usuario. Además, cada bioma contiene una serie de _props_ que también se distribuyen de forma aleatoria atendiendo a un porcentaje de probabilidad. 

## 📄 Descripción
Proyecto realizado para la asignatura de "Videojuegos" desarrollado durante el segundo año de Grado Superior de Desarrollo de Aplicaciones Multiplataforma + Perfil videojuegos y ocio digital (DAMvi).

## 💻 Tecnologías
![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=csharp&logoColor=white)
![Unity](https://img.shields.io/badge/unity-%23000000.svg?style=for-the-badge&logo=unity&logoColor=white)

## ⚙️ Funcionamiento
El funcionamiento se puede leer en el siguiente documento:
[Explicación perlin noise](https://gitlab.com/slimmyteam/unity/perlinproject/-/blob/main/PerlinProject/Assets/Explicaci%C3%B3Generaci%C3%B3Procedural.txt?ref_type=heads).

En el proyecto, si pulsamos la barra espaciadora, se genera un mapa procedural como se puede ver en el siguiente vídeo (clic en la imagen para verlo):

[![](http://img.youtube.com/vi/yZ0vnzYW7jk/0.jpg)](http://www.youtube.com/watch?v=yZ0vnzYW7jk "Mapa procedural")

En el vídeo se pueden ver los diferentes biomas, como serían el bosque (de color verde oscuro), las montañas heladas (color blanco), el agua (color azul claro), la hierba (verde claro), el campo de flores (color rosa), entre otros. 

También se pueden visualizar los diferentes _props_ como los muñecos de nieve (característico de las montañas heladas), las casas y vallas (característicos del bioma de tierra), las flores, arbustos, árboles, piedras, la lava...

## ©️ Desarrolladores
- Ethan Corchero Martín.
- Ana María Gómez León.
- Selene Milanés Rodríguez.

🎶 Música: _Perfect beauty_ por Zakhar Valaha.
