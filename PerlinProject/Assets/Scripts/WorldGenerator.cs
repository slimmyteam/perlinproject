using m17;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorldGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject m_CubePF;
    private GameObject[] m_Cubes = new GameObject[0];
    private List<GameObject> m_Props = new List<GameObject>();
    [SerializeField]
    private SOBioma[] m_Biomas;

    [Header("Size")]
    //size of the area we will paint
    [SerializeField]
    private float m_Amplitude = 50f;
    [SerializeField]
    private int m_Width;
    [SerializeField]
    private int m_Height;

    [Header("MAPA BIOMAS")]
    [Header("Base Parameters Bioma")]
    [SerializeField]
    //offset from the perlin map
    private float m_OffsetXBioma;
    [SerializeField]
    private float m_OffsetYBioma;
    [SerializeField]
    private float m_FrequencyBioma = 2f;

    //octaves
    private const int MAX_OCTAVESBioma = 8;
    [Header("Octave Parameters Bioma")]
    [SerializeField]
    [Range(0, MAX_OCTAVESBioma)]
    private int m_OctavesBioma = 0;
    [Range(2, 3)]
    [SerializeField]
    private int m_LacunarityBioma = 2;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_PersistenceBioma = 0.5f;
    [Tooltip("Do the octaves carve the terrain?")]
    [SerializeField]
    private bool m_CarveBioma = true;
    void Start()
    {
        GeneratePerlinMap();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GeneratePerlinMap();
        }
    }

    private void GeneratePerlinMap()
    {
        foreach (GameObject obj in m_Cubes)
            Destroy(obj);
        foreach (GameObject obj in m_Props)
            Destroy(obj);

        m_Cubes = new GameObject[m_Width * m_Height];
        Debug.Log("Calculant Perlin Noise");
        //recorrem el mapa
        for (int y = 0; y < m_Height; y++)
        {
            for (int x = 0; x < m_Width; x++)
            {
                float perlinNoise = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_FrequencyBioma, m_Width, m_Height, m_OffsetXBioma, m_OffsetYBioma, m_OctavesBioma, m_LacunarityBioma, m_PersistenceBioma, m_CarveBioma, false, true);
                SOBioma bioma;
                switch (perlinNoise)
                {
                    case float n when (n < 0.1f):
                        bioma = m_Biomas[0];
                        break;
                    case float n when (n < 0.2f):
                        bioma = m_Biomas[1];
                        break;
                    case float n when (n < 0.3f):
                        bioma = m_Biomas[2];
                        break;
                    case float n when (n < 0.4f):
                        bioma = m_Biomas[3];
                        break;
                    case float n when (n < 0.5f):
                        bioma = m_Biomas[4];
                        break;
                    case float n when (n < 0.6f):
                        bioma = m_Biomas[5];
                        break;
                    case float n when (n < 0.7f):
                        bioma = m_Biomas[6];
                        break;
                    case float n when (n < 0.8f):
                        bioma = m_Biomas[7];
                        break;
                    case float n when (n < 0.9f):
                        bioma = m_Biomas[8];
                        break;
                    case float n when (n < 1):
                        bioma = m_Biomas[9];
                        break;
                    default:
                        bioma = m_Biomas[0];
                        break;
                }

                m_Cubes[y * m_Width + x] = ColocarCubo(bioma, x, y);
            }
        }
    }

    private GameObject ColocarCubo(SOBioma bioma, int x, int y)
    {
        GameObject cubo = Instantiate(m_CubePF);
        float altura = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, bioma.frequency, m_Width, m_Height, bioma.offsetX, bioma.offsetY, bioma.octaves, bioma.lacunarity, bioma.persistence, bioma.carve, false, true) * bioma.amplitud;
        altura = Mathf.Floor(altura);
        cubo.transform.position = new Vector3(x, altura, y);
        Material cuboMaterial = cubo.GetComponentInChildren<MeshRenderer>().material;
        cuboMaterial.color = bioma.color;
        cubo.transform.localScale = new Vector3(1, altura, 1);
        GenerarProps(bioma, cubo, x, y);
        return cubo;
    }

    private void GenerarProps(SOBioma bioma, GameObject cubo, int x, int y)
    {
        float probability = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, bioma.frequencyProp, m_Width, m_Height, bioma.offsetX, bioma.offsetY, bioma.octavesProp, bioma.lacunarityProp, bioma.persistenceProp, bioma.carveProp, false, true) ;
        if(bioma.propsList.Length <=0)
            return;
        SOBioma.Prop prop = bioma.propsList.FirstOrDefault<SOBioma.Prop>(currentprop => currentprop.probability <= probability);
        if (prop.propPrefab == null) return;
        GameObject go = Instantiate(prop.propPrefab);
        go.transform.position = cubo.transform.position;
        m_Props.Add(go);
    }

}
