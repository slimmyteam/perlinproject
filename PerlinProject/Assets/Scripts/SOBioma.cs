using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="SOs")]
public class SOBioma : ScriptableObject
{
    public float amplitud;
    public Color color;
    [Header("MAPA S")]
    [Header("Base Parameters ")]
    public float offsetX;
    public float offsetY;
    public float frequency = 2f;

    //octaves
    public const int MAX_OCTAVES = 8;
    [Header("Octave Parameters ")]
    [Range(0, MAX_OCTAVES)]
    public int octaves = 0;
    [Range(2, 3)]
    public int lacunarity = 2;
    [Range(0.1f, 0.9f)]
    public float persistence = 0.5f;
    [Tooltip("Do the octaves carve the terrain?")]
    public bool carve = true;

    [Header("Props Parameters")]
    public float frequencyProp = 2f;
    [Header("Octave Parameters ")]
    [Range(0, MAX_OCTAVES)]
    public int octavesProp = 0;
    [Range(2, 3)]
    public int lacunarityProp = 2;
    [Range(0.1f, 0.9f)]
    public float persistenceProp = 0.5f;
    public bool carveProp = true;

    [Serializable]
    public struct Prop
    {
        public GameObject propPrefab;
        [Range(0f, 1f)]
        public float probability;
    }
    [Header("Props")]
    public Prop[] propsList;
}
